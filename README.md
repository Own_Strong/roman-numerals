
## Prerequisites

1. Java 1.8 
2. Apache Maven 3.5.0 or higher

---

## Quick Start

1. Open the command terminal
2. Clone this repository: *git clone git@bitbucket.org:Own_Strong/roman-numerals.git"*
3. Change directory (cd) to *"roman-numerals"* directory 
4. Run any of these commands:

    a. Run *"mvn clean spring-boot:run"* to run the application with Spring shell.

    b. Run *"mvn clean test"* to run all the unit tests 
    (located at *src/test/java/com/worldstack/math/romannumerals/services/RomanNumeralsImplTest.java*)


---

## Spring Shell Instructions:
After running *"mvn clean spring-boot:run"*, the application starts in Spring shell interactive mode.
To convert Roman numerals into a decimal format please enter:

*"romanToDecimal [NUMBER IN ROMAN NUMERAL FORMAT]"* 

(e.g. romanToDecimal MDCXLVII) 

The application will convert the Roman number into 
a decimal format and print the output to the screen. To get help please enter *"help"*. To close the application please enter *"exit"*


---

## Roman Numeral:

Roman numerals use the following symbols to express numbers:

  Symbol | Value
  :------|-----:
  I      |   1
  V      |   5
  X      |   10
  L      |   50
  C      |   100
  D      |   500
  M      |   1000
  V̅      |   5000
  X̅      |   10000
  L̅      |   50000
  C̅      |   100000
  D̅      |   500000
  M̅      |   1000000

Symbols must appear from the highest value to the lowest value.
In addition, these special combinations are used:

  Combo  | Value  | Instead of
  :------|------ :|:------------------
  IV     | 4      | (instead of IIII)
  IX     | 9      | (instead of VIIII)
  XL     | 40     | (instead of XXXX)
  XC     | 90     | (instead of LXXXX)
  CD     | 400    | (instead of CCCC)
  CM     | 900    | (instead of DCCCC)
  MV̅     | 4000   | (instead of MMMM)
  MX̅     | 9000   | (instead of V̅MMMM)
  X̅L̅     | 40000  | (instead of X̅X̅X̅X̅)
  X̅C̅     | 90000  | (instead of L̅X̅X̅X̅X̅)
  C̅D̅     | 400000 | (instead of C̅C̅C̅C̅)
  C̅M̅     | 900000 | (instead of M̅C̅C̅C̅C̅)
  
  
- The largest supported number is 3,999,999 (M̅M̅M̅C̅M̅X̅C̅MX̅CMXCIX).
- No symbol should ever appear more than three times in a row.  
- There is exactly one correct way of writing every number between 1 and 3,999,999.

For example, 

XXV -> 25

CMLXXVIII -> 978

V̅MMDCCCXC -> 7890 
 
C̅C̅C̅L̅V̅MCMXXVIII -> 356928



