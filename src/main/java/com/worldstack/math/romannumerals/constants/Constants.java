package com.worldstack.math.romannumerals.constants;

import java.util.*;

public class Constants {
    
    public static NavigableMap<Integer, String> ROMAN_DESCENDING_ORDER;
    private static TreeMap<Integer, String> ROMAN_NUMERALS = new TreeMap<>();

    static {
        ROMAN_NUMERALS.put(1, "I");
        ROMAN_NUMERALS.put(4, "IV");
        ROMAN_NUMERALS.put(5, "V");
        ROMAN_NUMERALS.put(9, "IX");
        ROMAN_NUMERALS.put(10, "X");
        ROMAN_NUMERALS.put(40, "XL");
        ROMAN_NUMERALS.put(50, "L");
        ROMAN_NUMERALS.put(90, "XC");
        ROMAN_NUMERALS.put(100, "C");
        ROMAN_NUMERALS.put(400, "CD");
        ROMAN_NUMERALS.put(500, "D");
        ROMAN_NUMERALS.put(900, "CM");
        ROMAN_NUMERALS.put(1000, "M");
        ROMAN_NUMERALS.put(4000, "MV̅");
        ROMAN_NUMERALS.put(5000, "V̅");
        ROMAN_NUMERALS.put(9000, "MX̅");
        ROMAN_NUMERALS.put(10000, "X̅");
        ROMAN_NUMERALS.put(40000, "X̅L̅");
        ROMAN_NUMERALS.put(50000, "L̅");
        ROMAN_NUMERALS.put(90000, "X̅C̅");
        ROMAN_NUMERALS.put(100000, "C̅");
        ROMAN_NUMERALS.put(400000, "C̅D̅");
        ROMAN_NUMERALS.put(500000, "D̅");
        ROMAN_NUMERALS.put(900000, "C̅M̅");
        ROMAN_NUMERALS.put(1000000, "M̅");
        ROMAN_DESCENDING_ORDER = ROMAN_NUMERALS.descendingMap();
    }
}