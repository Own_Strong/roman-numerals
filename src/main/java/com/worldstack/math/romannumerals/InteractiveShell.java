package com.worldstack.math.romannumerals;

import com.worldstack.math.romannumerals.services.RomanNumerals;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.shell.standard.ShellComponent;
import org.springframework.shell.standard.ShellMethod;
import org.springframework.shell.standard.ShellOption;

import javax.validation.constraints.Pattern;

@ShellComponent
public class InteractiveShell {

    @Autowired
    private RomanNumerals romanNumerals;

    @ShellMethod(value = "Convert Roman numerals to decimal. e.g romanToDecimal MDCXLVII -> 1647 ", key = "romanToDecimal")
    public String romanToDecimalConvertor(
            @Pattern(message = "You enter an invalid Roman number", regexp = "^(M̅){0,3}((C̅M̅)|(C̅D̅)|(D̅)?(C̅){0,3})((X̅C̅)|(X̅L̅)|(L̅)?(X̅){0,3})((MX̅)|(MV̅)|(V̅)?M{0,3})(CM|CD|D?C{0,3})(XC|XL|L?X{0,3})(IX|IV|V?I{0,3})$")
            @ShellOption() String text) throws Exception {

        return String.valueOf(romanNumerals.toDecimal(text));
    }
}