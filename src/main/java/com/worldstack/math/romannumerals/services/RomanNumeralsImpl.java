package com.worldstack.math.romannumerals.services;

import org.springframework.stereotype.Service;

import java.util.Map;

import static com.worldstack.math.romannumerals.constants.Constants.ROMAN_DESCENDING_ORDER;


@Service
public class RomanNumeralsImpl implements RomanNumerals {

    /**
     * Convert Roman number into decimal
     *
     * @param romanNumber Roman number. e.g.MDCCXXXV returns 1735
     * @return Decimal equivalent of Roman number
     * @throws Exception Throws an exception if Roman number is not valid
     */
    @Override
    public int toDecimal(String romanNumber) {
        if (romanNumber == null || romanNumber.isEmpty() || !romanNumber.matches("^(M̅){0,3}((C̅M̅)|(C̅D̅)|(D̅)?(C̅){0,3})((X̅C̅)|(X̅L̅)|(L̅)?(X̅){0,3})((MX̅)|(MV̅)|(V̅)?M{0,3})(CM|CD|D?C{0,3})(XC|XL|L?X{0,3})(IX|IV|V?I{0,3})$"))
            throw new IllegalArgumentException("Roman number is invalid");

        romanNumber = romanNumber.toUpperCase();
        int result = 0;

        for (Map.Entry<Integer, String> c : ROMAN_DESCENDING_ORDER.entrySet()) {
            String str = c.getValue();
            int am = c.getKey();
            while (romanNumber.startsWith(str)) {
                result += am;
                romanNumber = romanNumber.substring(str.length());
            }
        }
        return result;
    }
}