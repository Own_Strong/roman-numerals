package com.worldstack.math.romannumerals.services;

public interface RomanNumerals {

    int toDecimal(String romanNumber) throws Exception;
}
