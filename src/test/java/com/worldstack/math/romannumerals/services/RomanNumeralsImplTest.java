package com.worldstack.math.romannumerals.services;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static junit.framework.TestCase.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class RomanNumeralsImplTest {
    private static final Log LOGGER = LogFactory.getLog(RomanNumeralsImplTest.class);

    @Autowired
    private static RomanNumerals romanNumerals;

    @BeforeAll
    static void before() {
        romanNumerals = new RomanNumeralsImpl();
    }

    @Test
    void roman_toDecimal_valid_values_1_10_Test() throws Exception {
        LOGGER.info("\n************************************\nTest1 - Valid Roman number 1-10 \n");
        assertEquals(1, romanNumerals.toDecimal("I"));
        assertEquals(2, romanNumerals.toDecimal("II"));
        assertEquals(3, romanNumerals.toDecimal("III"));
        assertEquals(4, romanNumerals.toDecimal("IV"));
        assertEquals(5, romanNumerals.toDecimal("V"));
        assertEquals(6, romanNumerals.toDecimal("VI"));
        assertEquals(7, romanNumerals.toDecimal("VII"));
        assertEquals(8, romanNumerals.toDecimal("VIII"));
        assertEquals(9, romanNumerals.toDecimal("IX"));
        assertEquals(10, romanNumerals.toDecimal("X"));
    }

    @Test
    void roman_toDecimal_valid_values_10_40_Test() throws Exception {
        LOGGER.info("\n************************************\nTest2 - Valid Roman number 10-40 \n");
        assertEquals(11, romanNumerals.toDecimal("XI"));
        assertEquals(12, romanNumerals.toDecimal("XII"));
        assertEquals(13, romanNumerals.toDecimal("XIII"));
        assertEquals(14, romanNumerals.toDecimal("XIV"));
        assertEquals(15, romanNumerals.toDecimal("XV"));
        assertEquals(16, romanNumerals.toDecimal("XVI"));
        assertEquals(17, romanNumerals.toDecimal("XVII"));
        assertEquals(18, romanNumerals.toDecimal("XVIII"));
        assertEquals(19, romanNumerals.toDecimal("XIX"));
        assertEquals(20, romanNumerals.toDecimal("XX"));
        assertEquals(21, romanNumerals.toDecimal("XXI"));
        assertEquals(22, romanNumerals.toDecimal("XXII"));
        assertEquals(23, romanNumerals.toDecimal("XXIII"));
        assertEquals(24, romanNumerals.toDecimal("XXIV"));
        assertEquals(25, romanNumerals.toDecimal("XXV"));
        assertEquals(26, romanNumerals.toDecimal("XXVI"));
        assertEquals(27, romanNumerals.toDecimal("XXVII"));
        assertEquals(28, romanNumerals.toDecimal("XXVIII"));
        assertEquals(29, romanNumerals.toDecimal("XXIX"));
        assertEquals(30, romanNumerals.toDecimal("XXX"));
        assertEquals(31, romanNumerals.toDecimal("XXXI"));
        assertEquals(32, romanNumerals.toDecimal("XXXII"));
        assertEquals(33, romanNumerals.toDecimal("XXXIII"));
        assertEquals(34, romanNumerals.toDecimal("XXXIV"));
        assertEquals(35, romanNumerals.toDecimal("XXXV"));
        assertEquals(36, romanNumerals.toDecimal("XXXVI"));
        assertEquals(37, romanNumerals.toDecimal("XXXVII"));
        assertEquals(38, romanNumerals.toDecimal("XXXVIII"));
        assertEquals(39, romanNumerals.toDecimal("XXXIX"));
        assertEquals(40, romanNumerals.toDecimal("XL"));
    }

    @Test
    void roman_toDecimal_valid_values_40_90_Test() throws Exception {
        LOGGER.info("\n************************************\nTest3 - Valid Roman number 40-90 \n");
        assertEquals(41, romanNumerals.toDecimal("XLI"));
        assertEquals(42, romanNumerals.toDecimal("XLII"));
        assertEquals(43, romanNumerals.toDecimal("XLIII"));
        assertEquals(44, romanNumerals.toDecimal("XLIV"));
        assertEquals(45, romanNumerals.toDecimal("XLV"));
        assertEquals(46, romanNumerals.toDecimal("XLVI"));
        assertEquals(47, romanNumerals.toDecimal("XLVII"));
        assertEquals(48, romanNumerals.toDecimal("XLVIII"));
        assertEquals(49, romanNumerals.toDecimal("XLIX"));
        assertEquals(50, romanNumerals.toDecimal("L"));
        assertEquals(51, romanNumerals.toDecimal("LI"));
        assertEquals(52, romanNumerals.toDecimal("LII"));
        assertEquals(53, romanNumerals.toDecimal("LIII"));
        assertEquals(54, romanNumerals.toDecimal("LIV"));
        assertEquals(55, romanNumerals.toDecimal("LV"));
        assertEquals(56, romanNumerals.toDecimal("LVI"));
        assertEquals(57, romanNumerals.toDecimal("LVII"));
        assertEquals(58, romanNumerals.toDecimal("LVIII"));
        assertEquals(59, romanNumerals.toDecimal("LIX"));
        assertEquals(60, romanNumerals.toDecimal("LX"));
        assertEquals(61, romanNumerals.toDecimal("LXI"));
        assertEquals(62, romanNumerals.toDecimal("LXII"));
        assertEquals(63, romanNumerals.toDecimal("LXIII"));
        assertEquals(64, romanNumerals.toDecimal("LXIV"));
        assertEquals(65, romanNumerals.toDecimal("LXV"));
        assertEquals(66, romanNumerals.toDecimal("LXVI"));
        assertEquals(67, romanNumerals.toDecimal("LXVII"));
        assertEquals(68, romanNumerals.toDecimal("LXVIII"));
        assertEquals(69, romanNumerals.toDecimal("LXIX"));
        assertEquals(70, romanNumerals.toDecimal("LXX"));
        assertEquals(71, romanNumerals.toDecimal("LXXI"));
        assertEquals(72, romanNumerals.toDecimal("LXXII"));
        assertEquals(73, romanNumerals.toDecimal("LXXIII"));
        assertEquals(74, romanNumerals.toDecimal("LXXIV"));
        assertEquals(75, romanNumerals.toDecimal("LXXV"));
        assertEquals(76, romanNumerals.toDecimal("LXXVI"));
        assertEquals(77, romanNumerals.toDecimal("LXXVII"));
        assertEquals(78, romanNumerals.toDecimal("LXXVIII"));
        assertEquals(79, romanNumerals.toDecimal("LXXIX"));
        assertEquals(80, romanNumerals.toDecimal("LXXX"));
        assertEquals(81, romanNumerals.toDecimal("LXXXI"));
        assertEquals(82, romanNumerals.toDecimal("LXXXII"));
        assertEquals(83, romanNumerals.toDecimal("LXXXIII"));
        assertEquals(84, romanNumerals.toDecimal("LXXXIV"));
        assertEquals(85, romanNumerals.toDecimal("LXXXV"));
        assertEquals(86, romanNumerals.toDecimal("LXXXVI"));
        assertEquals(87, romanNumerals.toDecimal("LXXXVII"));
        assertEquals(88, romanNumerals.toDecimal("LXXXVIII"));
        assertEquals(89, romanNumerals.toDecimal("LXXXIX"));
        assertEquals(90, romanNumerals.toDecimal("XC"));
    }

    @Test
    void roman_toDecimal_valid_values_90_100_Test() throws Exception {
        LOGGER.info("\n************************************\nTest4 - Valid Roman number 9-100 \n");
        assertEquals(91, romanNumerals.toDecimal("XCI"));
        assertEquals(92, romanNumerals.toDecimal("XCII"));
        assertEquals(93, romanNumerals.toDecimal("XCIII"));
        assertEquals(94, romanNumerals.toDecimal("XCIV"));
        assertEquals(95, romanNumerals.toDecimal("XCV"));
        assertEquals(96, romanNumerals.toDecimal("XCVI"));
        assertEquals(97, romanNumerals.toDecimal("XCVII"));
        assertEquals(98, romanNumerals.toDecimal("XCVIII"));
        assertEquals(99, romanNumerals.toDecimal("XCIX"));
        assertEquals(100, romanNumerals.toDecimal("C"));
    }

    @Test
    void roman_toDecimal_valid_values_100_400_Test() throws Exception {
        LOGGER.info("\n************************************\nTest5 - Valid Roman number 100-400 \n");
        assertEquals(200, romanNumerals.toDecimal("CC"));
        assertEquals(300, romanNumerals.toDecimal("CCC"));
        assertEquals(400, romanNumerals.toDecimal("CD"));
    }

    @Test
    void roman_toDecimal_valid_values_400_900_Test() throws Exception {
        LOGGER.info("\n************************************\nTest6 - Valid Roman number 400-900 \n");
        assertEquals(500, romanNumerals.toDecimal("D"));
        assertEquals(600, romanNumerals.toDecimal("DC"));
        assertEquals(700, romanNumerals.toDecimal("DCC"));
        assertEquals(800, romanNumerals.toDecimal("DCCC"));
    }

    @Test
    void roman_toDecimal_valid_values_900_1000_Test() throws Exception {
        LOGGER.info("\n************************************\nTest7 - Valid Roman number 900-1000 \n");
        assertEquals(900, romanNumerals.toDecimal("CM"));
        assertEquals(1000, romanNumerals.toDecimal("M"));
    }

    @Test
    void roman_toDecimal_valid_values_1000_3999_Test() throws Exception {
        LOGGER.info("\n************************************\nTest8 - Valid Roman number 1000-3999 \n");
        assertEquals(1735, romanNumerals.toDecimal("MDCCXXXV"));
        assertEquals(2763, romanNumerals.toDecimal("MMDCCLXIII"));
        assertEquals(3999, romanNumerals.toDecimal("MMMCMXCIX"));
        assertEquals(3265, romanNumerals.toDecimal("MMMCCLXV"));
    }

    @Test
    void roman_toDecimal_valid_values_5000_40000_Test() throws Exception {
        LOGGER.info("\n************************************\nTest9 - Valid Roman number 5000-40000 \n");
        assertEquals(5000, romanNumerals.toDecimal("V̅"));
        assertEquals(10000, romanNumerals.toDecimal("X̅"));
        assertEquals(23463, romanNumerals.toDecimal("X̅X̅MMMCDLXIII"));
    }

    @Test
    void roman_toDecimal_valid_values_40000_90000_Test() throws Exception {
        LOGGER.info("\n************************************\nTest10 - Valid Roman number 40000-90000 \n");
        assertEquals(40000, romanNumerals.toDecimal("X̅L̅"));
        assertEquals(50000, romanNumerals.toDecimal("L̅"));
        assertEquals(78788, romanNumerals.toDecimal("L̅X̅X̅V̅MMMDCCLXXXVIII"));
    }

    @Test
    void roman_toDecimal_valid_values_90000_100000_Test() throws Exception {
        LOGGER.info("\n************************************\nTest11 - Valid Roman number 90000-100000 \n");
        assertEquals(90000, romanNumerals.toDecimal("X̅C̅"));
        assertEquals(95443, romanNumerals.toDecimal("X̅C̅V̅CDXLIII"));
    }

    @Test
    void roman_toDecimal_valid_values_100000_400000_Test() throws Exception {
        LOGGER.info("\n************************************\nTest12 - Valid Roman number 100000-400000 \n");
        assertEquals(100000, romanNumerals.toDecimal("C̅"));
        assertEquals(238973, romanNumerals.toDecimal("C̅C̅X̅X̅X̅V̅MMMCMLXXIII"));
    }

    @Test
    void roman_toDecimal_valid_values_400000_900000_Test() throws Exception {
        LOGGER.info("\n************************************\nTest13 - Valid Roman number 400000-900000 \n");
        assertEquals(400000, romanNumerals.toDecimal("C̅D̅"));
        assertEquals(500000, romanNumerals.toDecimal("D̅"));
        assertEquals(500034, romanNumerals.toDecimal("D̅XXXIV"));
    }

    @Test
    void roman_toDecimal_valid_values_900000_1000000_Test() throws Exception {
        LOGGER.info("\n************************************\nTest14 - Valid Roman number 900000-1000000 \n");
        assertEquals(900000, romanNumerals.toDecimal("C̅M̅"));
        assertEquals(900712, romanNumerals.toDecimal("C̅M̅DCCXII"));
    }

    @Test
    void roman_toDecimal_valid_values_1000000_3999999_Test() throws Exception {
        LOGGER.info("\n************************************\nTest15 - Valid Roman number 1000000-3999999 \n");
        assertEquals(1000000, romanNumerals.toDecimal("M̅"));
        assertEquals(2765429, romanNumerals.toDecimal("M̅M̅D̅C̅C̅L̅X̅V̅CDXXIX"));
        assertEquals(3999999, romanNumerals.toDecimal("M̅M̅M̅C̅M̅X̅C̅MX̅CMXCIX"));
    }

    @Test
    void roman_toDecimal_invalid_values_Tests() throws Exception {
        LOGGER.info("\n************************************\nTest16 - Invalid Roman number \n");
        Exception exception1 = assertThrows(IllegalArgumentException.class, () ->
                romanNumerals.toDecimal("MMMMM"));
        Exception exception2 = assertThrows(IllegalArgumentException.class, () ->
                romanNumerals.toDecimal("DCCLXMMMCMXCIX"));
        Exception exception3 = assertThrows(IllegalArgumentException.class, () ->
                romanNumerals.toDecimal("C̅M̅C̅M̅XMMD̅D̅CDXLIII"));
        assertEquals("Roman number is invalid", exception1.getMessage());
        assertEquals("Roman number is invalid", exception2.getMessage());
        assertEquals("Roman number is invalid", exception3.getMessage());
    }

    @Test
    void roman_toDecimal_empty_values_Tests() throws Exception {
        LOGGER.info("\n************************************\nTest17 - Empty Roman number \n");
        Exception exception = assertThrows(IllegalArgumentException.class, () ->
                romanNumerals.toDecimal(""));
        assertEquals("Roman number is invalid", exception.getMessage());
    }

    @Test
    void roman_toDecimal_null_values_Tests() throws Exception {
        LOGGER.info("\n************************************\nTest18 - Null Roman number \n");
        Exception exception = assertThrows(IllegalArgumentException.class, () ->
                romanNumerals.toDecimal(null));
        assertEquals("Roman number is invalid", exception.getMessage());
    }
}